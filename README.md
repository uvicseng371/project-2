# Project 2
### SENG 371 Spring 2019

## Team
- Jon Pavelich
- Tyler Harnadek
- Joel Kerfoot
- Sean Burt

## Project Summary
[ForestFyre](https://www.forestfyre.xyz) is an application which demos forest fire tracking based on satellite imagery. The app is split in to multiple repositories which are stored and hosted separately. The location and purpose of each portion are described below.

### Frontend
**Live:** https://www.forestfyre.xyz  
**Code:** https://gitlab.com/uvicseng371/project-2-frontend  
This is the web frontend that users see when interacting with the app. It is written in React and hosted on Azure Storage (using CloudFlare as a CDN).

### Backend
**Live:** https://api.forestfyre.xyz  
**Code:** https://gitlab.com/uvicseng371/project-2-backend  
This is the web backend (API) that the frontend uses to retrieve fire data to display. It is written in Python (using Flask) and hosted on Azure App Service (using CloudFlare as a CDN).

### Cloud Function
**Live:** *Not publicly accessible, timer based trigger*  
**Code:** https://gitlab.com/uvicseng371/project-2-cloudfunction  
This is a serverless function that runs every hour to generate fire data for the application to consume. It is written in Python and hosted on Azure Functions. This portion of the app is a black box - the rest of the application has no insight in to the implementation. It can be easily replaced by another cloud function. In a real deployment it will be replaced by a serverless function to actually process geospatial image data, but in this demo instance it randomly generates demo data.